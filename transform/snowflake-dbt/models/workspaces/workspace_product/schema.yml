version: 2

models:
  - name: wk_self_managed_spo
    description: Workspace model that builds a MVC for Self-Managed SPO and SPU KPIs.
    columns:
      - name: organization_id
        description: A unique id that is a combination of the host_name and dim_instance_id.
        tests:
          - not_null
      - name: delivery
        description: delivery is based on the product category name and is either SaaS or Self-Managed delivery.
        tests:
          - not_null
      - name: organization_type
        description: segmentation for organization type. Values - Group, Individual, User
        tests:
          - not_null
          - accepted_values:
              values: ['Group','Individual']
      - name: active_stage_count
        description: number of active stages for a given organization in a given month


  - name: wk_saas_spo
    description: Workspace model that shows monthly Stages per Organization data on a namespace-level for SaaS.
    columns:
      - name: reporting_month
        description: Month in which the product usage was reported.
        tests:
          - not_null
      - name: organization_id
        description: Top-level namespace id.
        tests:
          - not_null
      - name: delivery
        description: delivery is based on the product category name and is either SaaS or Self-Managed delivery.
        tests:
          - not_null
      - name: organization_type
        description: segmentation for organization type. Values - Group, Individual, User
        tests:
          - not_null
          - accepted_values:
              values: ['Group','Individual','User']
      - name: product_tier
        description: segmentation for Product Tier. Values - Core (CE, EE), Starter, Premium, Ultimate, NULL
        tests:
          - not_null
      - name: is_paid_product_tier
        description: Denotes whether the organization is on a paid product tier at the time of reporting.
        tests:
          - not_null
      - name: active_stage_count
        description: number of active stages for a given organization in a given month

  - name: wk_usage_ping_geo_node_usage
    description: Extract and flatten data from the geo nodes
    columns:
      - name: instance_path_id
        description: the unique ID for the combination of the ping_id and the path (unique service ping identifier and the metrics path).
        tests:
          - not_null
          - unique
      

  - name: fct_usage_event
    description: '{{ doc("fct_usage_event") }}'
    columns:
      - name: event_id
        description: The unique ID of an event.
        tests:
          - not_null
          - unique
      - name: event_name
        description: The name tied to an event
      - name: dim_instance_id
        description: The unique id of an instance from the dim_instance model.
      - name: dim_product_tier_id
        description: The unique id of a Product Tier in the dim_product_tier model.
      - name: dim_subscripton_id
        description: The unique id of a subscription from the dim_subscription model
      - name: dim_event_date_id
        description: The unique date id of an event to join to dim_date.
      - name: dim_crm_account_id
        description: The unique id of a crm account in the dim_crm_account model.
      - name: dim_billing_account_id
        description: The unqiue id of a billing account in the dim_billing_account model.
      - name: dim_namespace_id
        description: The unique id of the ultimate parent namespace from the dim_namespace model.
      - name: dim_project_id
        description: The unique id of a project from the dim_project model.
      - name: dim_user_id
        description: The unique id of a user from the dim_user model.
      - name: stage_name
        description: The name of the product stage (Stages are aligned logically starting with the 7 loop stages of Plan, Create, Verify, Package, Release, Configure, and Monitor. We then add the value stages that we talk about in marketing, these include Manage, Secure, and Protect. Lastly, we add the team stages, Ecosystem, Growth, and Enablement. These stages have groups that rally around the same users, GitLab integrators, GitLab itself, and GitLab administrators respectively.).
      - name: section_name
        description: The name of the section (Sections are a collection of stages. We attempt to align these logically along common workflows like Dev, Sec and Ops.).
      - name: group_name
        description: The name of the group (A stage has one or more groups.).
      - name: event_created_at
        description: The date at which the event was created
      - name: plan_id_at_event_date
        description: The id of the plan on the day the event was created.
      - name: plan_name_at_event_date
        description: The name of the plan type when the event was created
      - name: plan_was_paid_at_event_date
        description: Defines whether the plan was paid or free when the event was created
      - name: project_is_learn_gitlab
        description: Denotes whether the event is related to a Learn GitLab project, one created for the user during new user onboarding.
      - name: data_source
        description: The source application where the data was extracted from.
      - name: created_by
        description: Who created this model?
      - name: update_by
        description: Who updated this model last?
      - name: model_created_date
        description: The date at which the model was created
      - name: model_updated_date
        description: The date at which the model code was last updated

  - name: mart_usage_event
    description: '{{ doc("mart_usage_event") }}'
    columns:
      - name: event_id
        description: The unique ID of an event.
        tests:
          - not_null
          - unique
      - name: event_date
        description: The date when the event was created.
      - name: dim_user_id
        description: The unique id of a user in the dim_user model.
      - name: event_name
        description: The name tied to an event
      - name: dim_product_tier_id
        description: The unique id of a Product Tier in the dim_product_tier model.
      - name: dim_subscripton_id
        description: The unique id of a subscription from the dim_subscription model
      - name: dim_crm_account_id
        description: The unique id of a crm account in the dim_crm_account model.
      - name: dim_billing_account_id
        description: The unqiue id of a billing account in the dim_billing_account model.
      - name: stage_name
        description: The name of the product stage (Stages are aligned logically starting with the 7 loop stages of Plan, Create, Verify, Package, Release, Configure, and Monitor. We then add the value stages that we talk about in marketing, these include Manage, Secure, and Protect. Lastly, we add the team stages, Ecosystem, Growth, and Enablement. These stages have groups that rally around the same users, GitLab integrators, GitLab itself, and GitLab administrators respectively.).
      - name: section_name
        description: The name of the section (Sections are a collection of stages. We attempt to align these logically along common workflows like Dev, Sec and Ops.).
      - name: group_name
        description: The name of the group (A stage has one or more groups.).
      - name: data_source
        description: The source application where the data was extracted from.
      - name: plan_id_at_event_date
        description: The id of the plan on the day the event was created.
      - name: plan_name_at_event_date
        description: The name of the plan type when the event was created
      - name: plan_was_paid_at_event_date
        description: Defines whether the plan was paid or free when the event was created
      - name: dim_namespace_id
        description: The unique id of the ultimate parent namespace from the dim_namespace model.
      - name: dim_instance_id
        description: The unique id of an instance from the dim_instance model.
      - name: namespace_created_at
        description: The date when the namespace was created.
      - name: days_since_namespace_created
        description: The days since the namespace was created.
      - name: is_smau
        description: Denotes whether the event is identified as the stage's SMAU (stage monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: is_gmau
        description: Denotes whether the event is identified as the group's GMAU (group monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: is_umau
        description: Denotes whether the event is identified as the UMAU (unique monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: created_by
        description: Who created this model?
      - name: update_by
        description: Who updated this model last?
      - name: model_created_date
        description: The date at which the model was created
      - name: model_updated_date
        description: The date at which the model code was last updated
        

  - name: mart_usage_namespace_daily
    description: '{{ doc("mart_usage_namespace_daily") }}'
    columns:
      - name: mart_usage_namespace_id
        description: the unique ID combination of the event_date, event_name, na ddim_namespace_id in mart_usage_namespace.
        tests:
          - not_null
          - unique
      - name: event_date
        description: The date when the event was created.
      - name: event_name
        description: The name tied to an event
      - name: dim_product_tier_id
        description: The unique id of a Product Tier in the dim_product_tier model.
      - name: dim_subscripton_id
        description: The unique id of a subscription from the dim_subscription model
      - name: dim_crm_account_id
        description: The unique id of a crm account in the dim_crm_account model.
      - name: dim_billing_account_id
        description: The unqiue id of a billing account in the dim_billing_account model.
      - name: stage_name
        description: The name of the product stage (Stages are aligned logically starting with the 7 loop stages of Plan, Create, Verify, Package, Release, Configure, and Monitor. We then add the value stages that we talk about in marketing, these include Manage, Secure, and Protect. Lastly, we add the team stages, Ecosystem, Growth, and Enablement. These stages have groups that rally around the same users, GitLab integrators, GitLab itself, and GitLab administrators respectively.).
      - name: section_name
        description: The name of the section (Sections are a collection of stages. We attempt to align these logically along common workflows like Dev, Sec and Ops.).
      - name: group_name
        description: The name of the group (A stage has one or more groups.).
      - name: data_source
        description: The source application where the data was extracted from.
      - name: plan_id_at_event_date
        description: The id of the plan on the day the event was created.
      - name: plan_name_at_event_date
        description: The name of the plan type when the event was created
      - name: plan_was_paid_at_event_date
        description: Defines whether the plan was paid or free when the event was created
      - name: dim_instance_id
        description: The unique id of an instance from the dim_instance model.
      - name: dim_namespace_id
        description: The unique id of the ultimate parent namespace from the dim_namespace model.
      - name: namespace_created_at
        description: The date when the namespace was created.
      - name: days_since_namespace_created
        description: The days since the namespace was created.
      - name: is_smau
        description: Denotes whether the event is identified as the stage's SMAU (stage monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: is_gmau
        description: Denotes whether the event is identified as the group's GMAU (group monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: is_umau
        description: Denotes whether the event is identified as the UMAU (unique monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: event_count
        description: The count of events
      - name: distinct_user_count
        description: The count of distinct users
      - name: created_by
        description: Who created this model?
      - name: update_by
        description: Who updated this model last?
      - name: model_created_date
        description: The date at which the model was created
      - name: model_updated_date
        description: The date at which the model code was last updated

  - name: mart_usage_instance_daily
    description: '{{ doc("mart_usage_instance_daily") }}'
    columns:
      - name: mart_usage_instance_id
        description: the unique ID combination of the event_date, event_name, and dim_instance_id in mart_usage_instance.
        tests:
          - not_null
          - unique
      - name: event_date
        description: The date when the event was created.
      - name: event_name
        description: The name tied to an event
      - name: data_source
        description: The source application where the data was extracted from.
      - name: dim_instance_id
        description: The unique id of an instance from the dim_instance model.
      - name: event_count
        description: The count of events
      - name: distinct_user_count
        description: The count of distinct users
      - name: created_by
        description: Who created this model?
      - name: update_by
        description: Who updated this model last?
      - name: model_created_date
        description: The date at which the model was created
      - name: model_updated_date
        description: The date at which the model code was last updated

  - name: mart_usage_event_daily
    description: '{{ doc("mart_usage_event_daily") }}'
    columns:
      - name: mart_usage_event_daily_id
        description: the unique composite ID for the mart_usage_event_daily model consisting of event_date, dim_user_id, dim_namespace_id, and event_name.
        tests:
          - not_null
          - unique
      - name: event_date
        description: The date when the event was created.
      - name: dim_user_id
        description: The unique id of a user in the dim_user model.
      - name: dim_namespace_id
        description: The unique id of the ultimate parent namespace from the dim_namespace model.
      - name: dim_instance_id
        description: The unique id of an instance from the dim_instance model.
      - name: plan_id_at_event_date
        description: The id of the plan on the day the event was created.
      - name: event_name
        description: The name tied to an event
      - name: dim_product_tier_id
        description: The unique id of a Product Tier in the dim_product_tier model.
      - name: dim_subscripton_id
        description: The unique id of a subscription from the dim_subscription model
      - name: dim_crm_account_id
        description: The unique id of a crm account in the dim_crm_account model.
      - name: dim_billing_account_id
        description: The unqiue id of a billing account in the dim_billing_account model.
      - name: stage_name
        description: The name of the product stage (Stages are aligned logically starting with the 7 loop stages of Plan, Create, Verify, Package, Release, Configure, and Monitor. We then add the value stages that we talk about in marketing, these include Manage, Secure, and Protect. Lastly, we add the team stages, Ecosystem, Growth, and Enablement. These stages have groups that rally around the same users, GitLab integrators, GitLab itself, and GitLab administrators respectively.).
      - name: section_name
        description: The name of the section (Sections are a collection of stages. We attempt to align these logically along common workflows like Dev, Sec and Ops.).
      - name: group_name
        description: The name of the group (A stage has one or more groups.).
      - name: data_source
        description: The source application where the data was extracted from.
      - name: plan_name_at_event_date
        description: The name of the plan type when the event was created
      - name: plan_was_paid_at_event_date
        description: Defines whether the plan was paid or free when the event was created
      - name: namespace_is_internal
        description: Tells whether this namespace is internal or not
      - name: namespace_created_at
        description: The date when the nampespace was created.
      - name: days_since_namespace_created
        description: The days since the namespace was created.
      - name: days_since_namespace_creation_at_event_date
        description: The days since the namespace was created from the day the event was created.
      - name: is_smau
        description: Denotes whether the event is identified as the stage's SMAU (stage monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: is_gmau
        description: Denotes whether the event is identified as the group's GMAU (group monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: is_umau
        description: Denotes whether the event is identified as the UMAU (unique monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: event_count
        description: The count of events
      - name: created_by
        description: Who created this model?
      - name: update_by
        description: Who updated this model last?
      - name: model_created_date
        description: The date at which the model was created
      - name: model_updated_date
        description: The date at which the model code was last updated

  - name: mart_usage_event_plan_monthly
    description: '{{ doc("mart_usage_event_plan_monthly") }}'
    columns:
      - name: mart_usage_event_plan_monthly_id
        description: the unique ID of a montthly usage event plan in mart_usage_event_plan_monthly
        tests:
          - not_null
          - unique
      - name: reporting_month
        description: Month in which the product usage was reported. (this model only looks at the last 28 days of the month.)
      - name: plan_id_at_event_date
        description: The id of the plan on the day the event was created.
      - name: event_name
        description: The name tied to an event
      - name: stage_name
        description: The name of the product stage (Stages are aligned logically starting with the 7 loop stages of Plan, Create, Verify, Package, Release, Configure, and Monitor. We then add the value stages that we talk about in marketing, these include Manage, Secure, and Protect. Lastly, we add the team stages, Ecosystem, Growth, and Enablement. These stages have groups that rally around the same users, GitLab integrators, GitLab itself, and GitLab administrators respectively.).
      - name: section_name
        description: The name of the section (Sections are a collection of stages. We attempt to align these logically along common workflows like Dev, Sec and Ops.).
      - name: group_name
        description: The name of the group (A stage has one or more groups.).
      - name: is_smau
        description: Denotes whether the event is identified as the stage's SMAU (stage monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/#structure) for metric definitions
      - name: is_gmau
        description: Denotes whether the event is identified as the group's GMAU (group monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/#structure) for metric definitions
      - name: is_umau
        description: Denotes whether the event is identified as the UMAU (unique monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/#structure) for metric definitions
      - name: event_count
        description: The count of events
      - name: namespace_count
        description: The count of namespaces
      - name: user_count
        description: The count of users
      - name: created_by
        description: Who created this model?
      - name: update_by
        description: Who updated this model last?
      - name: model_created_date
        description: The date at which the model was created
      - name: model_updated_date
        description: The date at which the model code was last updated

  - name: prep_service_ping_instance
    description: ' {{ doc("prep_service_ping_instance") }} '
    columns:
      - name: dim_service_ping_instance_id
        tests:
          - not_null
          - unique

  - name: prep_service_ping_instance_flattened
    description: ' {{ doc("prep_service_ping_instance_flattened") }} '
    columns:
      - name: prep_service_ping_instance_flattened_id
        tests:
          - not_null
          - unique


  - name: fct_service_ping_instance
    description: ' {{ doc("fct_service_ping_instance") }} '
    columns:
      - name: fct_service_ping_instance_id
        tests:
          - not_null
          - unique

  - name: dim_service_ping_instance
    description: ' {{ doc("dim_service_ping_instance") }} '
    columns:
      - name: dim_service_ping_instance_id
        tests:
          - not_null
          - unique

  - name: mart_service_ping_instance
    description: ' {{ doc("dim_service_ping_instance") }} '
    columns:
      - name: mart_service_ping_instance_id
        tests:
          - not_null
          - unique

  - name: mart_xmau_metric_monthly
    description: '{{ doc("mart_xmau_metric_monthly") }}'
    columns:
      - name: mart_xmau_metric_monthly_id
        description: the unique ID of a montthly xMAU metric in mart_xmau_metric_monthly 
        tests:
          - not_null
          - unique
      - name: reporting_month
        description: Month in which the product usage was reported. (this model only looks at the last 28 days of the month.)
      - name: is_smau
        description: Denotes whether the event is identified as the stage's SMAU (stage monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: is_gmau
        description: Denotes whether the event is identified as the stage's GMAU (group monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: is_umau
        description: Denotes whether the event is identified as the stage's UMAU (unique monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: stage_name
        description: The name of the product stage (Stages are aligned logically starting with the 7 loop stages of Plan, Create, Verify, Package, Release, Configure, and Monitor. We then add the value stages that we talk about in marketing, these include Manage, Secure, and Protect. Lastly, we add the team stages, Ecosystem, Growth, and Enablement. These stages have groups that rally around the same users, GitLab integrators, GitLab itself, and GitLab administrators respectively.).
      - name: section_name
        description: The name of the section (Sections are a collection of stages. We attempt to align these logically along common workflows like Dev, Sec and Ops.).
      - name: group_name
        description: The name of the group (A stage has one or more groups.).
      - name: user_group
      - name: event_name_array
      - name: event_count
        description: The count of events
      - name: namespace_count
        description: The count of namespaces
      - name: user_count
        description: The count of users
      - name: created_by
        description: Who created this model?
      - name: update_by
        description: Who updated this model last?
      - name: model_created_date
        description: The date at which the model was created
      - name: model_updated_date
        description: The date at which the model code was last updated
